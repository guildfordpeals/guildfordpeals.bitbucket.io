function loadXMLDoc(filename)
{
if (window.ActiveXObject)
  {
  xhttp = new ActiveXObject("Msxml2.XMLHTTP");
  }
else
  {
  xhttp = new XMLHttpRequest();
  }
xhttp.open("GET", filename, false);
try {xhttp.responseType = "msxml-document"} catch(err) {} // Helping IE11
xhttp.send("");
return xhttp.responseXML;
}

function transform(object,xmlname)
{
xml = loadXMLDoc(xmlname);
xsl = loadXMLDoc(object.xslname);
// code for IE
if (window.ActiveXObject || xhttp.responseType == "msxml-document")
  {
  ex = xml.transformNode(xsl);
  document.getElementById(element).innerHTML = ex;
  }
// code for Chrome, Firefox, Opera, etc.
else if (document.implementation && document.implementation.createDocument)
  {
  xsltProcessor = new XSLTProcessor();
  xsltProcessor.importStylesheet(xsl);
  resultDocument = xsltProcessor.transformToFragment(xml, document);
  document.getElementById(object.element).appendChild(resultDocument);
  }
}

function addText(node,tag,text)
{
  var child = document.createElement(tag);
  child.appendChild(document.createTextNode(text));
  node.appendChild(child);
}

function makePage(xmlname)
{
  var tr = document.createElement("tr");
  addText(tr,'th',"Date");
  addText(tr,'th',"Method");
  addText(tr,'th',"Association");
  document.getElementById('index').appendChild(tr);

  document.index=new Object();
  document.index.xslname='pealindex.xsl';
  document.index.element='index'

  document.details=new Object();
  document.details.xslname='pealdetails.xsl';
  document.details.element='details'

  for (i = 0; i < arguments.length; i++) 
  {
    transform(document.index,arguments[i]);
    transform(document.details,arguments[i]);
  }
}

function latestUpdate()
{
  document.update=new Object();
  document.update.xslname='latest.xsl';
  for (i = 0; i < arguments.length; i++) 
  {
    document.update.element=arguments[i++]
    transform(document.update,arguments[i]);
  }
}
