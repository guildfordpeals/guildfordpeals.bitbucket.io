<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:bb="http://bb.ringingworld.co.uk/NS/performances#">
<xsl:template name="day-name">
        <xsl:param name="day"/>
        <xsl:if test="$day = 0">Sunday</xsl:if>
        <xsl:if test="$day = 1">Monday</xsl:if>
        <xsl:if test="$day = 2">Tuesday</xsl:if>
        <xsl:if test="$day = 3">Wednesday</xsl:if>
        <xsl:if test="$day = 4">Thursday</xsl:if>
        <xsl:if test="$day = 5">Friday</xsl:if>
        <xsl:if test="$day = 6">Saturday</xsl:if>
</xsl:template>
<xsl:template name="month-name">
        <xsl:param name="month"/>
        <xsl:if test="$month = 1">January</xsl:if>
        <xsl:if test="$month = 2">February</xsl:if>
        <xsl:if test="$month = 3">March</xsl:if>
        <xsl:if test="$month = 4">April</xsl:if>
        <xsl:if test="$month = 5">May</xsl:if>
        <xsl:if test="$month = 6">June</xsl:if>
        <xsl:if test="$month = 7">July</xsl:if>
        <xsl:if test="$month = 8">August</xsl:if>
        <xsl:if test="$month = 9">September</xsl:if>
        <xsl:if test="$month = 10">October</xsl:if>
        <xsl:if test="$month = 11">November</xsl:if>
        <xsl:if test="$month = 12">December</xsl:if>    
</xsl:template>
<xsl:template name="format-iso-date">
        <xsl:param name="iso-date"/>
        <xsl:variable name="year" select="substring($iso-date, 1, 4)"/>
        <xsl:variable name="month" select="substring($iso-date, 6, 2)"/>
        <xsl:variable name="day" select="substring($iso-date, 9, 2)"/>
        <xsl:variable name="month-name">
            <xsl:call-template name="month-name">
                <xsl:with-param name="month" select="$month"/>
            </xsl:call-template>
        </xsl:variable>
    <xsl:variable name="a" select="floor((14 - $month) div 12)"/>
    <xsl:variable name="y" select="$year + 4800 - $a"/>
    <xsl:variable name="m" select="$month + 12*$a - 3"/>
    <xsl:variable name="JDN" select="$day + floor((153*$m + 2) div 5) + 365*$y + floor($y div 4) - floor($y div 100) + floor($y div 400) - 32045" />
    <xsl:variable name="day-no" select="($JDN + 1) mod 7" />
        <xsl:variable name="day-name">
            <xsl:call-template name="day-name">
                <xsl:with-param name="day" select="$day-no"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="concat($day-name,', ',$day,' ',$month-name,' ',$year)"/>
    </xsl:template>
<xsl:template match="/">
<xsl:for-each select="bb:performances/bb:performance">   
<xsl:sort select="bb:date"/>
<a id="{@id}">
<p><b><xsl:value-of select="bb:association"/></b></p>
<p>On <xsl:call-template name="format-iso-date"><xsl:with-param name="iso-date" select="bb:date"/></xsl:call-template>
<!-- <xsl:if test="bb:duration != ''"> in <xsl:value-of select="bb:duration"/></xsl:if>--></p>
<p><b><xsl:value-of select="bb:title/bb:changes"/>&#160;<xsl:value-of select="bb:title/bb:method"/></b></p>
<p><xsl:value-of select="bb:details"/></p>
<xsl:for-each select="bb:composer">
<!--<xsl:if test=". != ''">-->
<p><!--<xsl:when test="@role == 'arranged'">Arranged</xsl:when><xsl:otherwise>Composed</xsl:otherwise>--> by <xsl:value-of select="."/></p>
<!--</xsl:if>-->
</xsl:for-each>

<table>
<xsl:for-each select="bb:ringers/bb:ringer">
<tr><td><xsl:value-of select="@bell"/></td><td><xsl:value-of select="."/><xsl:if test="@conductor"> (C)</xsl:if></td></tr>
</xsl:for-each>
</table>
<xsl:for-each select="bb:footnote">
<p><xsl:value-of select="."/></p>
</xsl:for-each>
</a>
<br/>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>


